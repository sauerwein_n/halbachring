from scipy.misc import factorial
import sympy as sym

xs = sym.Symbol('x', real = True)
ys = sym.Symbol('y', real = True)

def TaylorCoef(f, n = 1, z0 = 0):
    fa = [f]
    coef = [[sym.lambdify([xs,ys], f)(z0.real, z0.imag)]]
    for i in range(n):
        fa = [sym.diff(fa[0], xs)] + [sym.diff(f,ys) for f in fa]
        coef += [[sym.lambdify([xs,ys], f)(z0.real, z0.imag) for f in fa]]
    return coef

def Taylor(f, n = 1, z0 = 0):
    ct = TaylorCoef(f, n = n, z0 = z0)
    return sum([sum([ct[i][j]/factorial(j)/factorial(i - j)*xs**j * ys**(i-j)  for j in range(len(ct[i]))]) for i in range(len(ct))])

def getnthOrder(f, n,z0 = 0):
    ct = TaylorCoef(f, n = n, z0 = z0)
    return sum([ct[-1][j]/factorial(j)/factorial(n - j)*xs**(n - j) * ys**j  for j in range(len(ct[-1]))])

def Error2NPole(Elements, r_max, N = 1):
    
    
    phi = np.linspace(0, 2*np.pi, 1000)[:-1]
    r = np.linspace(0, r_max, 1000)[1:]
    
    PHI, R = np.meshgrid(phi, r)
    
    Z = R * np.exp(1j*PHI)
    
    Bnum = B(Elements,Z)
    Bsym = B(Elements,rs, sym = True)
    
    Bideal = sym.lambdify([xs, ys], getnthOrder(Bsym, n = N))(Z.real, Z.imag) 
    
    Error = np.abs((Bnum - Bideal))
    
    return (2*np.pi)/len(phi)*(r_max/len(r))*np.sum(Error*R) / (np.pi* r_max**2)  