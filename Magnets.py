import numpy as np
import matplotlib.pyplot as plt
import sympy as sym

class Stick:

    def __init__(self, R, B_r, z0):
        self.R = R
        self.B_r = B_r
        self.z0 = z0
    
    def plot(self, c = 1, ax = None):
        if ax == None:
            ax = plt.gca()

        from matplotlib.patches import Circle, Wedge, Polygon
        from matplotlib.collections import PatchCollection

        patches = [Circle((self.z0.real, self.z0.imag), self.R)]
        p = PatchCollection(patches, alpha=1)
        
        ax.add_collection(p)
        ax.quiver(self.z0.real, self.z0.imag, self.B_r.real, self.B_r.imag,width=1/50, units = 'xy',scale_units = 'xy', scale = 1/self.R*np.abs(self.B_r), pivot = 'mid', color = 'orange')
        ax.set_aspect('equal', 'datalim')
    
    def B(self, r):
        return np.where(np.abs(r - self.z0) > self.R, np.conjugate(self.B_r/2*self.R**2/(r-self.z0)**2),0)
    
    def symB(self, r):
        return sym.conjugate(self.B_r/2*self.R**2/(r-self.z0)**2)
    
class Halbachring:
    
    def __init__(self, ri, ro, B_r, m = 2, phi = 0, z0 = 0):
        self.ri = ri
        self.ro = ro
        self.m = m
        self.B_r = B_r
        self.phi = phi
        self.z0 = z0
        self.A = self.B_r * self.m /(self.m - 1) * (1 - (self.ri/self.ro)**(self.m-1))*1/self.ri**(self.m-1)
        
    def M(self, r):
        phi = np.angle(r)
        #for rr,pphi in zip(r,phi):
        #    print (rr,pphi)
        return np.exp(-1j*self.phi) *np.where(np.logical_and(self.ri < np.abs(r), self.ro > np.abs(r)), self.B_r * np.exp(1j * (self.m + 1) * (phi + self.phi)),0)
    
    def plot(self, c = 1, ax = None):
        if ax == None:
            ax = plt.gca()
        
        rr = (self.ri + self.ro)/2 * np.exp(1j* np.linspace(0, 2*np.pi, 17)[:-1])
        M = self.M(rr)

        from matplotlib.patches import Circle, Wedge, Polygon
        from matplotlib.collections import PatchCollection

        patches = [Wedge((self.z0.real, self.z0.imag), self.ro, 0, 360, width=self.ro-self.ri)]
        p = PatchCollection(patches, alpha=1)
        
        ax.add_collection(p)
        print (M)
        ax.quiver(rr.real, rr.imag, M.real, M.imag, pivot = 'mid', units = 'xy', scale_units = 'xy', scale = 2/(self.ro - self.ri)*np.abs(self.B_r), color = 'orange')
        ax.set_aspect('equal', 'datalim')
        
    def B(self, r):
        rr = np.exp(1j*self.phi) * r
        return np.where (np.abs(rr) < self.ri,np.exp(-1j*self.phi)* np.conj((rr/self.ri)**(self.m-1) * self.B_r * self.m /(self.m - 1) * (1 - (self.ri/self.ro)**(self.m-1))), self.M(r) )
    def symB(self,r):
        rr = np.exp(1j*self.phi) * r
        return np.exp(-1j*self.phi)* sym.conjugate((rr/self.ri)**(self.m-1) * self.B_r * self.m /(self.m - 1) * (1 - (self.ri/self.ro)**(self.m-1)))

def B(Elements, R, sym = False):
    if sym:
        return sum([p.symB(R) for p in Elements])
    return sum([p.B(R) for p in Elements])

def PlotModel(phalbach, x =  np.linspace(-1,1,10), y = np.linspace(-1,1,10), ax = None):

    X,Y = np.meshgrid(x,y)
    Z = X+1j*Y
    
    if ax == None:
        fig = plt.figure('Model')
        fig.clf()
        ax = plt.gca()
        
    Bnum = B(phalbach, Z)
    ax.quiver(Z.real, Z.imag, Bnum.real, Bnum.imag, pivot = 'mid', scale = 10, label = r'$\vec{B}$')
    [p.plot(ax = ax) for p in phalbach]
    plt.xlabel("x")
    plt.ylabel("y")
    ax.set_aspect('equal', 'datalim')
    
        